
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LucianoTadeu
 */
public class Pratica41 {
    public static void main(String[] args) {
        Elipse e = new Elipse(20, 15);
        Circulo c = new Circulo(15);
        
        System.out.println("Area Elipse = " + e.getArea());
        System.out.println("Perímetro Elipse = " + e.getPerimetro());
        System.out.println("Area Circulo = " + c.getArea());
        System.out.println("Perímetro Circulo = " + c.getPerimetro());
    } 
}
